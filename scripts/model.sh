#!/sbin/sh
# Written by Tkkg1994

getprop ro.boot.bootloader >> /tmp/BLmodel
ACTUAL_CSC=`cat /efs/imei/mps_code.dat`
ACTUAL_OMC=`cat /efs/imei/omcnw_code.dat`
SALES_CODE=`cat /system/omc/sales_code.dat`
sed -i -- "s/CSC=//g" /tmp/aroma/csc.prop
NEW_CSC=`cat /tmp/aroma/csc.prop`
aromabuildprop=/tmp/aroma/buildtweaks.prop
buildprop=/system/build.prop
floatingfeature=/system/etc/floating_feature.xml

if grep -q G93 /tmp/BLmodel; then
	mount /dev/block/platform/155a0000.ufs/by-name/SYSTEM /system
else
	mount /dev/block/platform/11120000.ufs/by-name/SYSTEM /system
fi

# Change build.prop to right model on the s7
if grep -q n8prop=1 $aromabuildprop; then
	if grep -q G935 /tmp/BLmodel; then
		echo "We use full n8 build.prop"
	else if grep -q G930 /tmp/BLmodel; then
		echo "We use full n8 build.prop"
	else
		echo "Not a s7 model, keeping stock"
	fi
	fi
else
	if grep -q G935 /tmp/BLmodel; then
		sed -i -- 's/ro.build.flavor=greatltexx-user/ro.build.flavor=hero2ltexx-user/g' $buildprop
		sed -i -- 's/ro.product.name=greatltexx/ro.product.name=hero2ltexx/g' $buildprop
		sed -i -- 's/ro.product.device=greatlte/ro.product.device=hero2lte/g' $buildprop
		if grep -q G935F /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-N950F/ro.product.model=SM-G935F/g' $buildprop
			echo "Changed to G935F"
		fi
		if grep -q G935K /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-N950F/ro.product.model=SM-G935K/g' $buildprop
			echo "Changed to G935K"
		fi
		if grep -q G935L /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-N950F/ro.product.model=SM-G935L/g' $buildprop
			echo "Changed to G935L"
		fi
		if grep -q G935S /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-N950F/ro.product.model=SM-G935S/g' $buildprop
			echo "Changed to G935S"
		fi
		if grep -q G935W8 /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-N950F/ro.product.model=SM-G935W8/g' $buildprop
			echo "Changed to G935W8"
		fi
	else if grep -q G930 /tmp/BLmodel; then
		sed -i -- 's/ro.build.flavor=greatltexx-user/ro.build.flavor=heroltexx-user/g' $buildprop
		sed -i -- 's/ro.product.name=greatltexx/ro.product.name=heroltexx/g' $buildprop
		sed -i -- 's/ro.product.device=greatlte/ro.product.device=herolte/g' $buildprop
		if grep -q G930F /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-N950F/ro.product.model=SM-G930F/g' $buildprop
			echo "Changed to G930F"
		fi
		if grep -q G930K /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-N950F/ro.product.model=SM-G930K/g' $buildprop
			echo "Changed to G930K"
		fi
		if grep -q G930L /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-N950F/ro.product.model=SM-G930L/g' $buildprop
			echo "Changed to G930L"
		fi
		if grep -q G930S /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-N950F/ro.product.model=SM-G930S/g' $buildprop
			echo "Changed to G930S"
		fi
		if grep -q G930W8 /tmp/BLmodel; then
			sed -i -- 's/ro.product.model=SM-N950F/ro.product.model=SM-G930W8/g' $buildprop
			echo "Changed to G930W8"
		fi
	else
		echo "Not a s7 model, keeping stock"
	fi
	fi
fi

if grep -q G93 /tmp/BLmodel; then
	sed -i -- "s/$ACTUAL_CSC/$NEW_CSC/g" /efs/imei/mps_code.dat
	rm -r /efs/imei/omcnw_code.dat
	cp -r /efs/imei/mps_code.dat /efs/imei/omcnw_code.dat
	chmod 0664 /efs/imei/omcnw_code.dat
	sed -i -- "s/$SALES_CODE/$NEW_CSC/g" /system/omc/sales_code.dat
else
	sed -i -- "s/$ACTUAL_CSC/$NEW_CSC/g" /efs/imei/mps_code.dat
	sed -i -- "s/$ACTUAL_OMC/$NEW_CSC/g" /efs/imei/omcnw_code.dat
	sed -i -- "s/$SALES_CODE/$NEW_CSC/g" /system/omc/sales_code.dat
fi

if grep -q G93 /tmp/BLmodel; then
	sed -i -- 's/8895/8890/g' $buildprop
	sed -i -- 's/abox/arizona/g' $buildprop
	sed -i -- 's/cortex-a53/cortex-a15/g' $buildprop
	sed -i -- 's/exynos-m2/exynos-m1/g' $buildprop
	echo "ro.exynos.dss=1" >>  $buildprop
	sed -i -- 's/16/0/g' $floatingfeature
	sed -i -- 's/3.5/0/g' $floatingfeature
fi

if grep -q G95 /tmp/BLmodel; then
	sed -i -- 's/cornerR:3.5/cornerR:0/g' $floatingfeature
fi

exit 10
