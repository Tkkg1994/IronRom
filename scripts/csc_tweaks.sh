#!/sbin/sh
# Written by Tkkg1994

CSC=`cat /tmp/aroma/csc.prop`
CSC_DIRECTORY=/system/omc
aromacscprop=/tmp/aroma/csctweaks.prop

getprop ro.boot.bootloader >> /tmp/BLmodel

mount /dev/block/platform/11120000.ufs/by-name/SYSTEM /system

if grep -q cameracall=1 $aromacscprop; then
	echo "# Camera during call"
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Camera_EnableCameraDuringCall>TRUE</CscFeature_Camera_EnableCameraDuringCall>' {} +
fi

if grep -q camerashutter=1 $aromacscprop; then
	echo "# Camera Shutter"
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Camera_ShutterSoundMenu>TRUE</CscFeature_Camera_ShutterSoundMenu>' {} +
fi

if grep -q airmessage=1 $aromacscprop; then
	echo "# AirMessage"
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Common_EnableAirMessage>TRUE</CscFeature_Common_EnableAirMessage>' {} +
fi

if grep -q savesms=1 $aromacscprop; then
	echo "# Save SMS"
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Message_EnableSaveRestoreSDCard>TRUE</CscFeature_Message_EnableSaveRestoreSDCard>' {} +
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Message_EnableSaveVMessage>TRUE</CscFeature_Message_EnableSaveVMessage>' {} +
fi

if grep -q disablemms=1 $aromacscprop; then
	echo "# Disable MMS convertion"
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Message_EnableMsgTypeIndicationDuringComposing>TRUE</CscFeature_Message_EnableMsgTypeIndicationDuringComposing>' {} +
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Message_DisableSmsToMmsConversionByTextInput>TRUE</CscFeature_Message_DisableSmsToMmsConversionByTextInput>' {} +
fi

if grep -q smstweaks=1 $aromacscprop; then
	echo "# SMS tweaks"
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Message_AddSendOptionInComposer>TRUE</CscFeature_Message_AddSendOptionInComposer>' {} +
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Message_EnableMenuMmsDeliveryTime>TRUE</CscFeature_Message_EnableMenuMmsDeliveryTime>' {} +
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Message_EnableSMSPcheckWhenSendSMS>TRUE</CscFeature_Message_EnableSMSPcheckWhenSendSMS>' {} +
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Message_ImageResizeQualityFactorWhenAttach>TRUE</CscFeature_Message_ImageResizeQualityFactorWhenAttach>' {} +
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_RIL_ForceConnectMMS>TRUE</CscFeature_RIL_ForceConnectMMS>' {} +
fi

if grep -q blockmenu=1 $aromacscprop; then
	echo "# Block Menu"
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/CscFeature_Setting_EnableMenuBlockCallMsg/d' {} +
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Message_EnableSpamFilteringEngine>TRUE</CscFeature_Message_EnableSpamFilteringEngine>' {} +
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Setting_EnableMenuBlockCallMsg>TRUE</CscFeature_Setting_EnableMenuBlockCallMsg>' {} +
fi

if grep -q networkspeed=1 $aromacscprop; then
	echo "# Networkspeed"
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Setting_SupportRealTimeNetworkSpeed>TRUE</CscFeature_Setting_SupportRealTimeNetworkSpeed>' {} +
fi

if grep -q lteonly=1 $aromacscprop; then
	echo "# LTE Only"
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Setting_CustNetworkSelMenu4>LTEONLY</CscFeature_Setting_CustNetworkSelMenu4>' {} +
fi

if grep -q smartmanager=1 $aromacscprop; then
	echo "# SmartManager"
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/CscFeature_SmartManager_ConfigSubFeatures/d' {} +
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_SmartManager_ConfigDashboard>dual_dashboard</CscFeature_SmartManager_ConfigDashboard>' {} +
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_SmartManager_ConfigSubFeatures>roguepopup|autoclean|autolaunch|autorestart|storageclean|backgroundapp|applock|notificationmanager|cstyle|fake_base_station|appclean</CscFeature_SmartManager_ConfigSubFeatures>' {} +
fi

if grep -q signalbar=1 $aromacscprop; then
	echo "# SignalBar"
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_SystemUI_ConfigMaxRssiLevel>5<\CscFeature_SystemUI_ConfigMaxRssiLevel>' {} +
fi

if grep -q recent=1 $aromacscprop; then
	echo "# Recent Protection"
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_SystemUI_SupportAssistanceAppChooser>TRUE</CscFeature_SystemUI_SupportAssistanceAppChooser>' {} +
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_SystemUI_SupportRecentAppProtection>TRUE</CscFeature_SystemUI_SupportRecentAppProtection>' {} +
fi

if grep -q datausage=1 $aromacscprop; then
	echo "# Data Usage"
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_SystemUI_SupportDataUsageViewOnQuickPanel>TRUE</CscFeature_SystemUI_SupportDataUsageViewOnQuickPanel>' {} +
fi

if grep -q recording=1 $aromacscprop; then
	echo "# Call recorder"
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_VoiceCall_ConfigRecording>RecordingAllowed</CscFeature_VoiceCall_ConfigRecording>' {} +
fi

if grep -q wifi=1 $aromacscprop; then
	echo "# Advanced Wifi"
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Wifi_SupportAdvancedMenu>TRUE</CscFeature_Wifi_SupportAdvancedMenu>' {} +
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Wifi_SupportMobileApOnTrigger>TRUE</CscFeature_Wifi_SupportMobileApOnTrigger>' {} +
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_Setting_SupportDataUsageReminder>TRUE</CscFeature_Setting_SupportDataUsageReminder>' {} +
fi

if grep -q nfc=1 $aromacscprop; then
	echo "# NFC Icon"
	find $CSC_DIRECTORY -type f -name "cscfeature.xml" -exec sed -i -- '/<FeatureSet>/ a\    <CscFeature_NFC_StatusBarIconType>DEFAULT</CscFeature_NFC_StatusBarIconType>' {} +
fi

exit 10

